using Godot;
using System;

public partial class main : Node {
	[Export] public PackedScene Boid { get; set; }

	public override void _Ready(){
		boid a_boid = Boid.Instantiate<boid>();
		AddChild(a_boid);
	}
}
