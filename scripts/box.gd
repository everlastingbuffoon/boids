extends Node

@export var topLeft_x: float
@export var topLeft_y: float
@export var botRight_x: float
@export var botRight_y: float

@export var neighbour_TL: Node
@export var neighbour_T: Node
@export var neighbour_TR: Node
@export var neighbour_L: Node
@export var neighbour_R: Node
@export var neighbour_BL: Node
@export var neighbour_B: Node
@export var neighbour_BR: Node

@export var currentBoids: Array[Node2D]

func inBoundry(pos):
	return(pos.x >= topLeft_x && pos.x <= botRight_x &&
		   pos.y <= topLeft_y && pos.y >= botRight_y)
