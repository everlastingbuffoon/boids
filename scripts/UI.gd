extends CanvasLayer

@export var button_popup: Button
@export var distance_popup: float
@export var button_popup_arrow: Polygon2D
var poped_up = false

@export var button_next: Button
@export var button_prev: Button

@export var pages: Array[Control]
var current_page = 0

#~ Page 0

@export var button_reset: Button

@export var slider_boid_counter: HSlider
@export var text_boid_counter: TextEdit

@export var draw_option: ItemList
@export var highlight_option: ItemList
@export var wall_option: ItemList

#~ Page 1

@export var slider_separation_radius: HSlider
@export var text_separation_radius: TextEdit

@export var slider_separation_factor: HSlider
@export var text_separation_factor: TextEdit

@export var slider_alignment_radius: HSlider
@export var text_alignment_radius: TextEdit

@export var slider_alignment_factor: HSlider
@export var text_alignment_factor: TextEdit

@export var slider_cohesion_radius: HSlider
@export var text_cohesion_radius: TextEdit

@export var slider_cohesion_factor: HSlider
@export var text_cohesion_factor: TextEdit

func _ready():
	button_popup.pressed.connect(self._on_button_popup_pressed)
	button_next.pressed.connect(self._on_button_next_pressed)
	button_prev.pressed.connect(self._on_button_prev_pressed)
	
	for i in pages.size():
		if i == current_page:
			pages[i].visible = true
		else:
			pages[i].visible = false
	
	if current_page == 0:
		button_prev.disabled = true
	if current_page == pages.size()-1:
		button_next.disabled = true
	
	#~ Page 0
	
	button_reset.pressed.connect(BoidController.reset)
	
	slider_boid_counter.value = BoidController.boid_count
	slider_boid_counter.value_changed.connect(self._on_slider_boids_change)
	text_boid_counter.text = str(BoidController.boid_count)
	text_boid_counter.text_changed.connect(self._on_text_boids_change)
	
	draw_option.select(BoidController.drawing_check)
	draw_option.item_selected.connect(self._on_list_draw_select)
	
	highlight_option.select(BoidController.highlight_check)
	highlight_option.item_selected.connect(self._on_list_highlight_select)
	
	wall_option.select(BoidController.wall_behaviour)
	wall_option.item_selected.connect(self._on_list_wall_select)
	
	#~ Page 1
	
	slider_separation_radius.value = BoidController.view_radius_separation
	slider_separation_radius.value_changed.connect(self._on_slider_separation_radius_change)
	text_separation_radius.text = str(BoidController.view_radius_separation)
	text_separation_radius.text_changed.connect(self._on_text_separation_radius_change)
	
	slider_separation_factor.value = BoidController.factor_separation
	slider_separation_factor.value_changed.connect(self._on_slider_separation_factor_change)
	text_separation_factor.text = str(BoidController.factor_separation)
	text_separation_factor.text_changed.connect(self._on_text_separation_factor_change)
	
	slider_alignment_radius.value = BoidController.view_radius_alignment
	slider_alignment_radius.value_changed.connect(self._on_slider_alignment_radius_change)
	text_alignment_radius.text = str(BoidController.view_radius_alignment)
	text_alignment_radius.text_changed.connect(self._on_text_alignment_radius_change)
	
	slider_alignment_factor.value = BoidController.factor_alignment
	slider_alignment_factor.value_changed.connect(self._on_slider_alignment_factor_change)
	text_alignment_factor.text = str(BoidController.factor_alignment)
	text_alignment_factor.text_changed.connect(self._on_text_alignment_factor_change)
	
	slider_cohesion_radius.value = BoidController.view_radius_cohesion
	slider_cohesion_radius.value_changed.connect(self._on_slider_cohesion_radius_change)
	text_cohesion_radius.text = str(BoidController.view_radius_cohesion)
	text_cohesion_radius.text_changed.connect(self._on_text_cohesion_radius_change)
	
	slider_cohesion_factor.value = BoidController.factor_cohesion
	slider_cohesion_factor.value_changed.connect(self._on_slider_cohesion_factor_change)
	text_cohesion_factor.text = str(BoidController.factor_cohesion)
	text_cohesion_factor.text_changed.connect(self._on_text_cohesion_factor_change)

#~ buttons ~#

func _on_button_popup_pressed():
	button_popup_arrow.scale.y = -button_popup_arrow.scale.y
	if !poped_up:
		offset.y -= distance_popup
		poped_up = true
	else:
		offset.y += distance_popup
		poped_up = false

func _on_button_next_pressed():
	if current_page < pages.size()-1:
		button_prev.disabled = false
		pages[current_page].visible = false
		current_page += 1
		pages[current_page].visible = true
		if current_page == pages.size()-1:
			button_next.disabled = true

func _on_button_prev_pressed():
	if current_page > 0:
		button_next.disabled = false
		pages[current_page].visible = false
		current_page -= 1
		pages[current_page].visible = true
		if current_page == 0:
			button_prev.disabled = true

#~ lists ~#

func _on_list_draw_select(index):
	BoidController.drawing_check = index
	
func _on_list_highlight_select(index):
	BoidController.highlight_check = index
	if index == 1:
		BoidController.boid_list[0].change_color(Color(1, 0, 0, 1))
	else:
		BoidController.boid_list[0].change_color(Color(1, 1, 1, 1))

func _on_list_wall_select(index):
	BoidController.wall_behaviour = index

#~ sliders & text ~#

func _on_slider_boids_change(new_value):
	BoidController.boid_count = new_value
	text_boid_counter.text = str(new_value)
	
func _on_text_boids_change(new_value):
	var val = float(new_value)
	if val < slider_boid_counter.min_value:
		val = slider_boid_counter.min_value
	if val > slider_boid_counter.max_value:
		val = slider_boid_counter.max_value
		
	BoidController.boid_count = val
	slider_boid_counter.value = val

func _on_slider_separation_radius_change(new_value):
	BoidController.view_radius_separation = new_value
	text_separation_radius.text = str(new_value)
	
func _on_text_separation_radius_change(new_value):
	var val = float(new_value)
	if val < slider_separation_radius.min_value:
		val = slider_separation_radius.min_value
	if val > slider_separation_radius.max_value:
		val = slider_separation_radius.max_value
		
	BoidController.view_radius_separation = val
	slider_separation_radius.value = val

func _on_slider_separation_factor_change(new_value):
	BoidController.factor_separation = new_value
	text_separation_factor.text = str(new_value)
	
func _on_text_separation_factor_change(new_value):
	var val = float(new_value)
	if val < slider_separation_factor.min_value:
		val = slider_separation_factor.min_value
	if val > slider_separation_factor.max_value:
		val = slider_separation_factor.max_value
		
	BoidController.factor_separation = val
	slider_separation_factor.value = val
	
func _on_slider_alignment_radius_change(new_value):
	BoidController.view_radius_alignment = new_value
	text_alignment_radius.text = str(new_value)
	
func _on_text_alignment_radius_change(new_value):
	var val = float(new_value)
	if val < slider_alignment_radius.min_value:
		val = slider_alignment_radius.min_value
	if val > slider_alignment_radius.max_value:
		val = slider_alignment_radius.max_value
		
	BoidController.view_radius_alignment = val
	slider_alignment_radius.value = val

func _on_slider_alignment_factor_change(new_value):
	BoidController.factor_alignment = new_value
	text_alignment_factor.text = str(new_value)
	
func _on_text_alignment_factor_change(new_value):
	var val = float(new_value)
	if val < slider_alignment_factor.min_value:
		val = slider_alignment_factor.min_value
	if val > slider_alignment_factor.max_value:
		val = slider_alignment_factor.max_value
		
	BoidController.factor_alignment = val
	slider_alignment_factor.value = val
	
func _on_slider_cohesion_radius_change(new_value):
	BoidController.view_radius_cohesion = new_value
	text_cohesion_radius.text = str(new_value)
	
func _on_text_cohesion_radius_change(new_value):
	var val = float(new_value)
	if val < slider_cohesion_radius.min_value:
		val = slider_cohesion_radius.min_value
	if val > slider_cohesion_radius.max_value:
		val = slider_cohesion_radius.max_value
		
	BoidController.view_radius_cohesion = val
	slider_cohesion_radius.value = val

func _on_slider_cohesion_factor_change(new_value):
	BoidController.factor_cohesion = new_value
	text_cohesion_factor.text = str(new_value)
	
func _on_text_cohesion_factor_change(new_value):
	var val = float(new_value)
	if val < slider_cohesion_factor.min_value:
		val = slider_cohesion_factor.min_value
	if val > slider_cohesion_factor.max_value:
		val = slider_cohesion_factor.max_value
		
	BoidController.factor_cohesion = val
	slider_cohesion_factor.value = val
