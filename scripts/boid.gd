extends Node2D

@export var triangle: Polygon2D
@export var separation_code: Vector2i
@export var alignment_code: Vector2i
@export var cohesion_code: Vector2i
@export var calculation_done: bool

var velocity

func _ready():
	var random = RandomNumberGenerator.new()
	random.randomize()
	
	var starting_speed = random.randf_range(BoidController.min_speed, BoidController.max_speed)
	var dir = random.randf_range(0, PI * 2)

	var vel = Vector2(starting_speed, 0)
	velocity = vel.rotated(dir)
	rotation = dir

func _process(delta):
	if calculation_done:
		return
	
	var vel = velocity

	# edge behaviour

	if position.x > BoidController.screen_size.x/2 - BoidController.edge_distance_steer || \
	   position.x < BoidController.edge_distance_steer - BoidController.screen_size.x/2 || \
	   position.y > BoidController.screen_size.y/2 - BoidController.edge_distance_steer || \
	   position.y < BoidController.edge_distance_steer - BoidController.screen_size.y/2:
		if BoidController.wall_behaviour == 0:
			vel += steer_away_from_edge()
		elif BoidController.wall_behaviour == 1:
			position = wrapped_position()
		elif BoidController.wall_behaviour == 2:
			vel = bounce_edge()
	
	# main calculations
	
	vel += separation()
	vel += alignment()
	vel += cohesion()
	
	# speed control
	
	if vel.length() > BoidController.max_speed:
		vel.x = (vel.x/vel.length()) * BoidController.max_speed
		vel.y = (vel.y/vel.length()) * BoidController.max_speed
		
	if vel.length() < BoidController.min_speed:
		vel.x = (vel.x/vel.length()) * BoidController.min_speed
		vel.y = (vel.y/vel.length()) * BoidController.min_speed

	velocity = vel
	rotation = vel.angle()
	
	# final movement

	position += velocity * delta
	
	# drawing phase
	
	queue_redraw()
	
	# aaand done!
	
	calculation_done = true

#~ separation calculation ~#

func separation():
	var separation_x = 0
	var separation_y = 0
	
	var temp_boid_list = []
	if BoidController.view_radius_separation > 0:
		temp_boid_list.append_array(BoidController.grid_separation[separation_code.x][separation_code.y])
		if separation_code.x > 0:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x-1][separation_code.y])
		if separation_code.x < BoidController.grid_separation.size()-1:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x+1][separation_code.y])
		if separation_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x][separation_code.y-1])
		if separation_code.y < BoidController.grid_separation[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x][separation_code.y+1])
		if separation_code.x > 0 && separation_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x-1][separation_code.y-1])
		if separation_code.x < BoidController.grid_separation.size()-1 && separation_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x+1][separation_code.y-1])
		if separation_code.x > 0 && separation_code.y < BoidController.grid_separation[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x-1][separation_code.y+1])
		if separation_code.x < BoidController.grid_separation.size()-1 && separation_code.y < BoidController.grid_separation[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_separation[separation_code.x+1][separation_code.y+1])
	else:
		temp_boid_list = []
	
	#for i in range(temp_boid_list.size()):
		#for j in range(temp_boid_list.size()):
			#if i != j and temp_boid_list[i] == temp_boid_list[j]:
				#print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
	
	for boid in temp_boid_list:
		if boid != self:
			if(position.distance_to(boid.position) <= BoidController.view_radius_separation):
				separation_x += position.x - boid.position.x
				separation_y += position.y - boid.position.y

	separation_x = separation_x * BoidController.factor_separation
	separation_y = separation_y * BoidController.factor_separation
	
	return Vector2(separation_x, separation_y)

#~ alignment calculation ~#

func alignment():
	var alignment_x = 0
	var alignment_y = 0
	var neighboring_boids = 0

	var temp_boid_list = []
	if BoidController.view_radius_alignment > 0:
		temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x][alignment_code.y])
		if alignment_code.x > 0:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x-1][alignment_code.y])
		if alignment_code.x < BoidController.grid_alignment.size()-1:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x+1][alignment_code.y])
		if alignment_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x][alignment_code.y-1])
		if alignment_code.y < BoidController.grid_alignment[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x][alignment_code.y+1])
		if alignment_code.x > 0 && alignment_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x-1][alignment_code.y-1])
		if alignment_code.x < BoidController.grid_alignment.size()-1 && alignment_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x+1][alignment_code.y-1])
		if alignment_code.x > 0 && alignment_code.y < BoidController.grid_alignment[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x-1][alignment_code.y+1])
		if alignment_code.x < BoidController.grid_alignment.size()-1 && alignment_code.y < BoidController.grid_alignment[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_alignment[alignment_code.x+1][alignment_code.y+1])
	else:
		temp_boid_list = []
		
	for boid in temp_boid_list:
		if boid != self:
			if(position.distance_to(boid.position) <= BoidController.view_radius_alignment):
				alignment_x += boid.velocity.x
				alignment_y += boid.velocity.y
				neighboring_boids += 1
	
	if neighboring_boids > 0:
		alignment_x = (alignment_x/neighboring_boids - position.x) * BoidController.factor_alignment
		alignment_y = (alignment_y/neighboring_boids - position.y) * BoidController.factor_alignment
	
	return Vector2(alignment_x, alignment_y)

#~ cohesion calculation ~#

func cohesion():
	var cohesion_x = 0
	var cohesion_y = 0
	var neighboring_boids = 0

	var temp_boid_list = []
	if BoidController.view_radius_cohesion > 0:
		temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x][cohesion_code.y])
		if cohesion_code.x > 0:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x-1][cohesion_code.y])
		if cohesion_code.x < BoidController.grid_cohesion.size()-1:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x+1][cohesion_code.y])
		if cohesion_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x][cohesion_code.y-1])
		if cohesion_code.y < BoidController.grid_cohesion[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x][cohesion_code.y+1])
		if cohesion_code.x > 0 && cohesion_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x-1][cohesion_code.y-1])
		if cohesion_code.x < BoidController.grid_cohesion.size()-1 && cohesion_code.y > 0:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x+1][cohesion_code.y-1])
		if cohesion_code.x > 0 && cohesion_code.y < BoidController.grid_cohesion[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x-1][cohesion_code.y+1])
		if cohesion_code.x < BoidController.grid_cohesion.size()-1 && cohesion_code.y < BoidController.grid_cohesion[0].size()-1:
			temp_boid_list.append_array(BoidController.grid_cohesion[cohesion_code.x+1][cohesion_code.y+1])
	else:
		temp_boid_list = []

	for boid in temp_boid_list:
		if boid != self:
			if(position.distance_to(boid.position) <= BoidController.view_radius_cohesion):
				cohesion_x += boid.position.x
				cohesion_y += boid.position.y
				neighboring_boids += 1
	
	if neighboring_boids > 0:
		cohesion_x = (cohesion_x/neighboring_boids - position.x) * BoidController.factor_cohesion
		cohesion_y = (cohesion_y/neighboring_boids - position.y) * BoidController.factor_cohesion
	
	return Vector2(cohesion_x, cohesion_y)

#~ wall avoiding ~#

func steer_away_from_edge():
	var steer_x = 0
	var steer_y = 0
	
	if position.x > BoidController.screen_size.x/2 - BoidController.edge_distance_steer:
		steer_x -= BoidController.power_steer
	elif position.x < BoidController.edge_distance_steer - BoidController.screen_size.x/2:
		steer_x += BoidController.power_steer

	if position.y > BoidController.screen_size.y/2 - BoidController.edge_distance_steer:
		steer_y -= BoidController.power_steer
	elif position.y < BoidController.edge_distance_steer - BoidController.screen_size.y/2:
		steer_y += BoidController.power_steer
		
	return Vector2(steer_x, steer_y)

#~ edge of screen wrapping ~#

func wrapped_position():
	var new_x = position.x;
	var new_y = position.y;

	if new_x > BoidController.screen_size.x/2:
		new_x = -BoidController.screen_size.x/2
	elif new_x < -BoidController.screen_size.x/2:
		new_x = BoidController.screen_size.x/2

	if new_y > BoidController.screen_size.y/2:
		new_y = -BoidController.screen_size.y/2
	elif new_y < -BoidController.screen_size.y/2:
		new_y = BoidController.screen_size.y/2

	return Vector2(new_x, new_y)

#~ edge of screen bouncing ~#

func bounce_edge():
	var vel_x = velocity.x
	var vel_y = velocity.y
	
	if position.x > BoidController.screen_size.x/2:
		position.x = BoidController.screen_size.x/2
		vel_x= -vel_x
	elif position.x < -BoidController.screen_size.x/2:
		position.x = -BoidController.screen_size.x/2
		vel_x = -vel_x

	if position.y > BoidController.screen_size.y/2:
		position.y = BoidController.screen_size.y/2
		vel_y = -vel_y
	elif position.y < -BoidController.screen_size.y/2:
		position.y = -BoidController.screen_size.y/2
		vel_y = -vel_y
		
	return Vector2(vel_x, vel_y)

#~ art corner ~#

func change_color(c):
	triangle.set_color(c)

func draw_circle_arc(center, radius, angle_from, angle_to, color):
	var nb_points = 32
	var points_arc = PackedVector2Array()

	for i in range(nb_points + 1):
		var angle_point = deg_to_rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color)

func _draw():
	if (BoidController.drawing_check == 1 && BoidController.boid_list[0] == self) || BoidController.drawing_check == 2:
		draw_circle_arc(Vector2(0, 0), BoidController.view_radius_separation, 0, 360, Color(1.0, 0.0, 0.0))
		draw_circle_arc(Vector2(0, 0), BoidController.view_radius_alignment, 0, 360, Color(0.0, 1.0, 0.0))
		draw_circle_arc(Vector2(0, 0), BoidController.view_radius_cohesion, 0, 360, Color(0.0, 0.0, 1.0))
