extends Node

@export var topLeft_x: float
@export var topLeft_y: float
@export var botRight_x: float
@export var botRight_y: float

@export var currentBoids: Array[Node2D]

@export var chlidTopLeft: Node
@export var chlidTopRight: Node
@export var chlidBotLeft: Node
@export var chlidBotRight: Node

@export var minDist: float

func insert(boid):
	if (boid == null):
		return
	
	if (abs(topLeft_x - botRight_x) <= minDist) && (abs(topLeft_y - botRight_y) <= minDist):
		currentBoids.append(boid)
		return
	
	if (topLeft_x + botRight_x)/2 >= boid.position.x:
		if (topLeft_y + botRight_y)/2 >= boid.position.y:
			if chlidTopLeft == null:
				chlidTopLeft = BoidController.Quad.instantiate()
				chlidTopLeft.topLeft_x = topLeft_x
				chlidTopLeft.topLeft_y = topLeft_y
				chlidTopLeft.botRight_x = (topLeft_x + botRight_x) / 2
				chlidTopLeft.botRight_y = (topLeft_y + botRight_y) / 2
				chlidTopLeft.minDist = minDist
			chlidTopLeft.insert(boid)
		else:
			if chlidBotLeft == null:
				chlidBotLeft = BoidController.Quad.instantiate()
				chlidBotLeft.topLeft_x = topLeft_x
				chlidBotLeft.topLeft_y = (topLeft_y + botRight_y) / 2
				chlidBotLeft.botRight_x = (topLeft_x + botRight_x) / 2
				chlidBotLeft.botRight_y = botRight_y
				chlidBotLeft.minDist = minDist
			chlidBotLeft.insert(boid)
	else:
		if (topLeft_y + botRight_y)/2 >= boid.position.y:
			if chlidTopRight == null:
				chlidTopRight = BoidController.Quad.instantiate()
				chlidTopRight.topLeft_x = (topLeft_x + botRight_x) / 2
				chlidTopRight.topLeft_y = topLeft_y
				chlidTopRight.botRight_x = botRight_x
				chlidTopRight.botRight_y = (topLeft_y + botRight_y) / 2
				chlidTopRight.minDist = minDist
			chlidTopRight.insert(boid)
		else:
			if chlidBotRight == null:
				chlidBotRight = BoidController.Quad.instantiate()
				chlidBotRight.topLeft_x = (topLeft_x + botRight_x) / 2
				chlidBotRight.topLeft_y = (topLeft_y + botRight_y) / 2
				chlidBotRight.botRight_x = botRight_x
				chlidBotRight.botRight_y = botRight_y
				chlidBotRight.minDist = minDist
			chlidBotRight.insert(boid)

func inBoundry(pos):
	return(pos.x >= topLeft_x && pos.x <= botRight_x &&
		   pos.y <= topLeft_y && pos.y >= botRight_y)
		
func delete_tree():
	if chlidTopLeft != null:
		chlidTopLeft.delete_tree()
		chlidTopLeft.queue_free()
	if chlidBotLeft != null:
		chlidBotLeft.delete_tree()
		chlidBotLeft.queue_free()
	if chlidTopRight != null:
		chlidTopRight.delete_tree()
		chlidTopRight.queue_free()
	if chlidBotRight != null:
		chlidBotRight.delete_tree()
		chlidBotRight.queue_free()

func find_cluster(centre_x, centre_y, range):
	if currentBoids.size() > 0:
		return currentBoids
	
	var temp_boid_list: Array
	
	if chlidTopLeft != null:
		if (abs(topLeft_x - centre_x) < range) && (abs(topLeft_y - centre_y) < range):
			temp_boid_list.append_array(chlidTopLeft.find_cluster(centre_x, centre_y, range))
			
	if chlidTopRight != null:
		if (abs(botRight_x - centre_x) < range) && (abs(topLeft_y - centre_y) < range):
			temp_boid_list.append_array(chlidTopRight.find_cluster(centre_x, centre_y, range))
			
	if chlidBotLeft != null:
		if (abs(topLeft_x - centre_x) < range) && (abs(botRight_y - centre_y) < range):
			temp_boid_list.append_array(chlidBotLeft.find_cluster(centre_x, centre_y, range))
			
	if chlidBotRight != null:
		if (abs(botRight_x - centre_x) < range) && (abs(botRight_y - centre_y) < range):
			temp_boid_list.append_array(chlidBotRight.find_cluster(centre_x, centre_y, range))
			
	return temp_boid_list
