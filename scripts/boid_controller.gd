extends Node

@export var Boid: PackedScene
@export var Quad: PackedScene
@export var boid_count = 100

@export var screen_size: Vector2

@export var min_speed: float
@export var max_speed: float

@export var view_radius_separation: float
@export var view_radius_alignment: float
@export var view_radius_cohesion: float

@export var factor_separation: float
@export var factor_alignment: float
@export var factor_cohesion: float

@export var edge_distance_steer: float
@export var power_steer: float

@export var wall_behaviour: int

@export var drawing_check: int
@export var highlight_check: int

@export var boid_list: Array

#@export var quad_tree_separation: Node
#@export var quad_tree_alignment: Node
#@export var quad_tree_cohesion: Node

@export var grid_separation: Array
@export var grid_alignment: Array
@export var grid_cohesion: Array

var random

func reset():
	for i in boid_list.size():
		boid_list[0].queue_free()
		boid_list.remove_at(0)

func _ready():
	random = RandomNumberGenerator.new()
	random.randomize()

func _process(delta):
	var boid_countdown = boid_list.size()
	for boid in boid_list:
		if boid.calculation_done:
			boid_countdown -= 1
	
	if boid_countdown > 0:
		return
		
	for boid in boid_list:
		boid.calculation_done = false
	
	# Set Screen Size
	
	screen_size = get_viewport().get_visible_rect().size
	
	# Manage Boid Count
	
	if boid_list.size() < boid_count:
		var a_boid = Boid.instantiate()
		add_child(a_boid)
		BoidController.boid_list.append(a_boid)
		a_boid.position = Vector2(random.randf_range(-screen_size.x/2, screen_size.x/2), \
								  random.randf_range(-screen_size.y/2, screen_size.y/2))
	elif boid_list.size() > boid_count:
		boid_list[0].queue_free()
		boid_list.remove_at(0)
	
	# Create Structures
	
	var temp_max_i
	var temp_max_j
	
	grid_separation = []
	temp_max_i = ceil(screen_size.x/view_radius_separation)+1
	temp_max_j = ceil(screen_size.y/view_radius_separation)+1
	for i in range(temp_max_i):
		grid_separation.append([])
		for j in range(temp_max_j):
			grid_separation[i].append([])
			
	# print("!", temp_max_i, ", ", temp_max_j, "!")
	
	grid_alignment = []
	temp_max_i = ceil(screen_size.x/view_radius_alignment)+1
	temp_max_j = ceil(screen_size.y/view_radius_alignment)+1
	for i in range(temp_max_i):
		grid_alignment.append([])
		for j in range(temp_max_j):
			grid_alignment[i].append([])
	
	grid_cohesion = []
	temp_max_i = ceil(screen_size.x/view_radius_cohesion)+1
	temp_max_j = ceil(screen_size.y/view_radius_cohesion)+1
	for i in range(temp_max_i):
		grid_cohesion.append([])
		for j in range(temp_max_j):
			grid_cohesion[i].append([])
	
	# print(grid_separation[0].size())
	
	for boid in boid_list:
		if view_radius_separation > 0:
			grid_separation[floor((boid.position.x+screen_size.x/2)/view_radius_separation)][floor(-(boid.position.y-screen_size.y/2)/view_radius_separation)].append(boid)
			boid.separation_code.x = floor((boid.position.x+screen_size.x/2)/view_radius_separation)
			boid.separation_code.y = floor(-(boid.position.y-screen_size.y/2)/view_radius_separation)
		else:
			boid.separation_code.x = -1
			boid.separation_code.y = -1
		
		if view_radius_alignment > 0:
			grid_alignment[floor((boid.position.x+screen_size.x/2)/view_radius_alignment)][floor(-(boid.position.y-screen_size.y/2)/view_radius_alignment)].append(boid)
			boid.alignment_code.x = floor((boid.position.x+screen_size.x/2)/view_radius_alignment)
			boid.alignment_code.y = floor(-(boid.position.y-screen_size.y/2)/view_radius_alignment)
		else:
			boid.alignment_code.x = -1
			boid.alignment_code.y = -1
		
		if view_radius_cohesion > 0:
			grid_cohesion[floor((boid.position.x+screen_size.x/2)/view_radius_cohesion)][floor(-(boid.position.y-screen_size.y/2)/view_radius_cohesion)].append(boid)
			boid.cohesion_code.x = floor((boid.position.x+screen_size.x/2)/view_radius_cohesion)
			boid.cohesion_code.y = floor(-(boid.position.y-screen_size.y/2)/view_radius_cohesion)
		else:
			boid.cohesion_code.x = -1
			boid.cohesion_code.y = -1
