using Godot;
using System;

public partial class boid : RigidBody2D {
	[Export] public int speed { get; set; } = 400;
	public Vector2 ScreenSize;
	
	private Vector2 StartingVelocity;
	
	public override void _Ready(){
		var random = new RandomNumberGenerator();
		random.Randomize();
		ScreenSize = GetViewportRect().Size;
		
		float dir = (float)GD.RandRange(0, Mathf.Pi * 2);
		
		Position = new Vector2(0, 0);
		Rotation = dir;

		Vector2 vel = new Vector2(speed, 0);
		LinearVelocity = vel.Rotated(dir);
		//ApplyImpulse(vel.Rotated(dir), Position);
		StartingVelocity = LinearVelocity;
	}

	public override void _Process(double delta){
		if(Position.X > ScreenSize.X/2 ||
		   Position.X < -ScreenSize.X/2 ||
		   Position.Y > ScreenSize.Y/2 ||
		   Position.Y < -ScreenSize.Y/2)
			Position = WrappedPosition();
		//LinearVelocity = StartingVelocity;
	}
	
	public Vector2 WrappedPosition(){
		float new_x = Position.X;
		if(new_x > ScreenSize.X/2) new_x = -ScreenSize.X/2 + 10;
		if(new_x < -ScreenSize.X/2) new_x = ScreenSize.X/2 - 10;
		
		float new_y = Position.Y;
		if(new_y > ScreenSize.Y/2) new_y = -ScreenSize.Y/2 + 10;
		if(new_y < -ScreenSize.Y/2) new_y = ScreenSize.Y/2 - 10;
		
		return new Vector2(new_x, new_y);
	}
}
